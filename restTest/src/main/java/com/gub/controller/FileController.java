/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gub.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author abra
 */
@RestController
public class FileController 
{

    @PostMapping("/upload")
    String uploadFileHandler(@RequestParam("name") String name,
                            @RequestParam("file") MultipartFile file) 
    {

        if(!file.isEmpty()) 
        {
            try 
            {
                byte[] bytes = file.getBytes();

                // Creating the directory to store file
                String rootPath = System.getProperty("catalina.home");
                File dir = new File("test");
                
                if (!dir.exists()) 
                {
                    dir.mkdirs();
                }

                File serverFile = new File("/home/abra/" + name);
                System.out.println("serverFile path: " + serverFile.getAbsolutePath());

                BufferedOutputStream stream = new BufferedOutputStream(
                new FileOutputStream(serverFile));
                stream.write(bytes);
                stream.close();

                return "You successfully uploaded file=" + name + " & path to saved -> " + serverFile.getAbsolutePath();
            } 
            catch (Exception e) 
            {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
            
        } 
        else 
        {
            return "You failed to upload " + name
                    + " because the file was empty.";
        }
    }
    
    
//  https://stackoverflow.com/questions/42671074/delete-image-from-local-path-using-spring-mvc

    @DeleteMapping("/delete")
    String deleteFile(@RequestParam("path") String path) 
    {
        File file = new File(path);
        
        try 
        {

            if (file.delete()) 
            {
                System.out.println(file.getName() + " is deleted!");
            } 
            else 
            {
                System.out.println("Delete operation is failed.");
            }
        } 
        catch (Exception e) 
        {
            System.out.println("Failed to Delete image !!");
        }
        
        return file.getName() + " is deleted";
        
    }

}
