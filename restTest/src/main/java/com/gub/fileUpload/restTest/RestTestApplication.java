package com.gub.fileUpload.restTest;

import com.gub.controller.FileController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ComponentScan(basePackageClasses = FileController.class)
public class RestTestApplication 
{

	public static void main(String[] args) 
        {
		SpringApplication.run(RestTestApplication.class, args);
                System.out.println("done!!!!");
	}
        
}
